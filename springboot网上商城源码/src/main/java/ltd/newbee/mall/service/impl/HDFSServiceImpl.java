package ltd.newbee.mall.service.impl;

import ltd.newbee.mall.dao.HDFSDao;
import ltd.newbee.mall.service.HDFSService;
import ltd.newbee.mall.util.TimeUtil;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Properties;
@Service
public class HDFSServiceImpl implements HDFSService {

    @Autowired
    HDFSDao hdfsDao;
    @Override
    public void dataToHDFS(String line) {
        FileSystem fileSystem = hdfsDao.getFileSystem();
        Properties properties = hdfsDao.getProperties();
        System.out.println(properties.getProperty("spring.data.hdfs.log"));
        //
        FSDataOutputStream outputStream;
        try {
            if(fileSystem.exists(new Path(properties.getProperty("spring.data.hdfs.log")+ TimeUtil.getTimes2()+".log"))){
                //存在
                outputStream = fileSystem.append(new Path(properties.getProperty("spring.data.hdfs.log")+TimeUtil.getTimes2()+".log"));
            }else{
                outputStream = fileSystem.create(new Path(properties.getProperty("spring.data.hdfs.log")+TimeUtil.getTimes2()+".log"));
            }
            //输出流   文件不存在  就会被创建出来   内容会覆盖
            //InputStream in, OutputStream out, Configuration conf, boolean close
            //输出流   文件不存在  不会被创建出来   内容不会覆盖
            IOUtils.copyBytes(org.apache.commons.io.IOUtils.toInputStream(line+"\r\n"),outputStream,hdfsDao.getConfiguration(),true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
