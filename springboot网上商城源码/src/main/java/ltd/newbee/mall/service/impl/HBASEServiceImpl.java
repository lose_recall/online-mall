package ltd.newbee.mall.service.impl;

import ltd.newbee.mall.dao.HBASEDao;
import ltd.newbee.mall.entity.log.ShoppingLog;
import ltd.newbee.mall.service.HBASEService;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@Service
public class HBASEServiceImpl implements HBASEService {
    @Autowired
    HBASEDao hbaseDao;
    @Override
    public void dataToHBASE(ShoppingLog shoppingLog) {
        Connection connection=hbaseDao.getConnection();
        Properties properties = hbaseDao.getProperties();

        try {
            Table table=connection.getTable(TableName.valueOf(properties.getProperty("spring.data.hbase.table")));

            String rowKey=shoppingLog.getPhone()+"--"+new Date().getTime();
            Put put = new Put(Bytes.toBytes(rowKey));
            //向Put对象中组装数据
            put.addColumn(Bytes.toBytes(properties.getProperty("spring.data.hbase.columFamily")), Bytes.toBytes("name"), Bytes.toBytes(shoppingLog.getUsername()));
            put.addColumn(Bytes.toBytes(properties.getProperty("spring.data.hbase.columFamily")), Bytes.toBytes("phone"), Bytes.toBytes(shoppingLog.getPhone()));
            put.addColumn(Bytes.toBytes(properties.getProperty("spring.data.hbase.columFamily")), Bytes.toBytes("title"), Bytes.toBytes(shoppingLog.getTitle()));
            put.addColumn(Bytes.toBytes(properties.getProperty("spring.data.hbase.columFamily")), Bytes.toBytes("price"), Bytes.toBytes(shoppingLog.getPrice()+""));
            put.addColumn(Bytes.toBytes(properties.getProperty("spring.data.hbase.columFamily")), Bytes.toBytes("times"), Bytes.toBytes(shoppingLog.getTimes()));


            table.put(put);
            table.close();
            System.out.println("插入数据成功");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
