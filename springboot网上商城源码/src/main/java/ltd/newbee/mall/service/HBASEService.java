package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.log.ShoppingLog;

public interface HBASEService {
    void dataToHBASE(ShoppingLog shoppingLog);
}
