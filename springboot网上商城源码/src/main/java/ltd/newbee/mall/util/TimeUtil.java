package ltd.newbee.mall.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    public static String getTimes(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format=simpleDateFormat.format(new Date());
        return format;
    }

    public static String getTimes2() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format=simpleDateFormat.format(new Date());
        return format;
    }
}
