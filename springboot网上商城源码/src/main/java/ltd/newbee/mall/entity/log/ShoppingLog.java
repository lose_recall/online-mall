package ltd.newbee.mall.entity.log;

public class ShoppingLog {
    private String username;
    private String phone;
    private float price;
    private String title;
    private String times;

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ShoppingLog(String username, String phone, float price, String title, String times) {
        this.username = username;
        this.phone = phone;
        this.price = price;
        this.title = title;
        this.times = times;
    }

    @Override
    public String toString() {
        return "ShoppingLog{" +
                "username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", price=" + price +
                ", title='" + title + '\'' +
                ", times='" + times + '\'' +
                '}';
    }
}
