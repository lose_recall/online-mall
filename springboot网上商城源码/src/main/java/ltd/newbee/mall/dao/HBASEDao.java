package ltd.newbee.mall.dao;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;

@Service
public class HBASEDao {
    static Configuration conf;
    static Connection connection;
    static Properties properties;
    static{
        properties=new Properties();
        try {
            properties.load(HBASEDao.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", properties.getProperty("spring.data.hbase.url"));
        conf.set("hbase.zookeeper.property.clientPort", properties.getProperty("spring.data.hbase.port"));
        try {
            connection = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 获取hadoop的连接对象
     */
    public  Connection getConnection(){
        return connection;
    }

    /**
     * 获取hadoop的属性
     */
    public  Properties getProperties(){
        return properties;
    }


    /**
     * 获取hadoop的配置
     */
    public  Configuration getConfiguration(){
        return conf;
    }
}
