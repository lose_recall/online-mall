package ltd.newbee.mall.dao;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;

@Service
public class HDFSDao {
    static FileSystem fileSystem;
    static Configuration configuration;
    static Properties properties;
    static{
        configuration=new Configuration();
        properties=new Properties();
        try {
            properties.load(HDFSDao.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        configuration.set("dfs.client.block.write.replace-datanode-on-failure.enable", "true");
        configuration.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");
        try {
            fileSystem=FileSystem.get(URI.create(properties.getProperty("spring.data.hdfs.url")), configuration,"root");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取hadoop的连接对象
     */
    public  FileSystem getFileSystem(){
        return fileSystem;
    }

    /**
     * 获取hadoop的属性
     */
    public  Properties getProperties(){
        return properties;
    }


    /**
     * 获取hadoop的配置
     */
    public  Configuration getConfiguration(){
        return configuration;
    }
}
